import java.util.Scanner;
public class Airfryer{
  private String brand;
  private double changeInTemperature; 
  private int temperature; 
  private int time; 
  
  public String displayTamperatureAndTime(){
   String userAttention = "Your air fryer is set on " + temperature + "degrees for " +time + "minutes. ";
   return userAttention;
  }
  public String changeTemperature(){
    
    double newTemperature = temperature+changeInTemperature;
    String newTemperatureMessage = "The new temperature is " + newTemperature;
   
    
    return newTemperatureMessage;
  }
  
  public int cookingTime(int timeInMinutes){
    timeInMinutes=aHelperMethodForCookinTime(timeInMinutes);
    this.time=timeInMinutes;
       return this.time;
    }
  
  
  private int aHelperMethodForCookinTime(int timeInMinutes){
     if(timeInMinutes >= 30){
       return timeInMinutes;
     } else {
       while(timeInMinutes<30){
         System.out.println("The machine can be set for more that 30 minutes. change the timer please");
         Scanner keyboard=new Scanner(System.in);
         timeInMinutes=keyboard.nextInt();}
           ;
       return timeInMinutes;

}
  }
  
  //setters 
  public void setTheBrand(String theNewBrand){
     this.brand = theNewBrand;
   }

   public void setTheTemperature(int theNewTemperature){
    this.temperature = theNewTemperature;
   }
    
    public void setTheTime(int theNewTime){
      this.time = theNewTime;
    }
    
    //getters
    
    public String getTheBrand(){
    return this.brand;
  }
    
  public int getTheTemperature(){
    return this.temperature;
  }
  
  public int getTheTime(){
    return this.time;
  }
  
  public double getTheChangeInTemperature(){
    return this.changeInTemperature;
  }
  
  //constructor
  public Airfryer(String brand, int temperature, int time){
    this.brand=brand;
    this.temperature=temperature;
    this.time=time;
    }
}

 